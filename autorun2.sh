#!/bin/bash

# Check if Python 3 is installed
python3_version=$(python3 -V 2>&1)
if [[ $python3_version == *"Python 3"* ]]; then
  echo "Python 3 is already installed with version: $python3_version"
else
  # Install Python 3 if not already installed
  echo "Python 3 is not installed, installing now..."
  sudo apt-get update
  sudo apt-get install python3 python3-pip -y
  echo "Python 3 is now installed with version: $(python3 -V 2>&1)"
fi

# Install PyPI dependencies
wget https://gitlab.com/sangeeth.kavinthya/ova-automation-vmaas-it-staging/-/raw/main/requirements.txt -O requirements.txt
python3 -m pip install -r requirements.txt

# Changing directory to /home/$USER/monitorscripts
mkdir -p ~/monitorscripts && cd ~/monitorscripts

# Getting the script from gitlab
wget https://gitlab.com/sangeeth.kavinthya/ova-automation-vmaas-it-staging/-/raw/main/extract-v2.py -O extract-v2.py

# Change permissions
chmod 600 extract-v2.py

# Run the script
python3 extract-v2.py
